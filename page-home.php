<?php
/*
  Template Name: Homepage
*/

#Hero Section
$schoolName  	= get_field('school_name');
$semName 			= get_field('seminar_name');
$semAddr 			= get_field('address_of_event');
$semDate 			= get_field('date_of_seminar');
$semImg 			= get_field('event_feature_image');

#Abous Us Section
$aboutTitle   = get_field('about_us_title');
$aboutBody    = get_field('about_us_body');

#Social Media
$facebook     = get_field('facebook');
$instagram    = get_field('instagram');
$twitter      = get_field('twitter');
$googlePlus   = get_field('google_plus');

#Contact From
$contactForm  = do_shortcode('[contact-form-7 id="29" title="Contact Us"]');
$sponsorForm  = do_shortcode('[contact-form-7 id="31" title="Sponsor Form"]');

#Packages
$bronzePrice     = get_field('bronze_price');
$bronzeBenefits  = get_field('bronze_benefits');
$silverPrice     = get_field('silver_price');
$silverBenefits  = get_field('silver_benefits');
$goldPrice       = get_field('gold_price');
$goldBenefits    = get_field('gold_benefits');

#Section Header
$speakerSection  = get_field('speaker_section_heading');
$eventSection    = get_field('event_section_heading');
$sponsorSection  = get_field('sponser_section_heading');
$contactSecton   = get_field('contact_section_heading');

#Map Section
$mapHeading      = get_field('map_section_title') ;
$map             = get_field('map');

#Section Toggles
$addSponsorSection  = get_field('add_sponsor_section');

get_header(); ?>

	<!-- Hero  -->
  <div class="header" role="banner">
    <div class="container-fluid">
      <h1 class="header_heading text-white"><span class="header_heading-small"><?php echo $schoolName; ?></span><br><?php echo $semName; ?></h1>
      <ul class="ul-nolist header-info">
        <li><i class="header_vect">address</i><p><?php echo $semAddr; ?></p></li>
        <li><i class="header_vect">date</i><p><?php echo $semDate; ?></p></li>
      </ul>
      <button class="btn btn-yellow" id="learnMore">Learn More</button>
    </div>
    <div class="header_img js_para" data-speed="20"><img src="<?php echo $semImg['url']; ?>" alt="<?php echo $semImg['alt']; ?>"></div>
  </div>

<!-- About Us Section  -->
  <section class="section section-padding100 js_para" data-speed="25">
    <div class="container">
      <h2><?php echo $aboutTitle; ?></h2>
      <p><?php echo $aboutBody; ?></p>
    </div>
  </section>

    <!-- Speaker Section  -->
  <section class="section sectionSpeaker section-paddingBottom100 js_para" data-speed="25">
    <div class="container">
      <h2><?php echo $speakerSection; ?></h2>

      <ul class="row ul-nolist">

        <?php $loop = new WP_Query( array ( 'post_type' => 'speakers', 'orderby' => 'post_id', 'order' => 'ASC' ) );?>

        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
          <li class="col-lg-3 col-sm-6 col-xs-12 col-12">
            <div class="speaker_img" style="background: url('<?php echo get_field('speaker_image') ?>') center top/cover no-repeat;"></div>
            <h3 class="heading-bold heading-h4"><?php echo get_field('speaker_name'); ?></h3>
            <p class="p-noMargin p-font16"><?php echo get_field('speaker_company'); ?></p>
            <p class="p-noMargin p-font16"><?php echo get_field('speaker_position'); ?></p>
          </li>
        <?php endwhile; ?>
      </ul>
    </div>
  </section>

  <!-- Event Section  -->
  <section class="section sectionEvent text-white section-padding100">
    <div class="container js_para" data-speed="25">
      <h2><?php echo $eventSection; ?></h2>
      <?php $loop = new WP_Query( array ( 'post_type' => 'events', 'orderby' => 'post_id', 'order' => 'ASC' ) );?>

      <?php while ($loop->have_posts()) : $loop->the_post(); ?>
        <ul class="row ul-nolist block-alignLeft">
          <li class="col-lg-6 col-sm-12 sem-img" style="background: url(<?php echo get_field('event_image') ;?>) center/cover no-repeat; background-clip: content-box;"></li>
          <li class="col-lg-6 col-sm-12">
            <h3><?php the_title(); ?></h3>
            <p><?php echo get_field('event_description'); ?></p>
          </li>
        </ul>
      <?php endwhile; ?>
    </div>
  </section>

  <!-- Google Map  -->
  <section class="section section-paddingBottom100">
    <div class="container">
      <h2><?php echo $mapHeading; ?></h2>
      <?php if(!empty($map)): ?>
        <div class="acf-map">
	       <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
       </div>
      <?php endif; ?>
    </div>
  </section>

<?php if ($addSponsorSection):?>

  <!-- Sponsor Section  -->
  <section class="sectionSponsor section section-paddingBottom100">
    <div class="container">
      <h2><?php echo $sponsorSection; ?></h2>
      <ul class="row sponsor ul-nolist">
        <?php $loop = new WP_Query( array ( 'post_type' => 'sponsors', 'orderby' => 'post_id', 'order' => 'ASC' ) );?>
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>
          <li class="sponsorSection_logo col-lg-3 col-sm-4"><a target="_blank" href="<?php echo get_field('sponsor_website'); ?>"><img src="<?php echo get_field('sponsor_logo'); ?>" alt="<?php the_title(); ?>"></a></li>
        <?php endwhile; ?>
      </ul>
      <p class="text-center"><button class="btn btn-yellow btn-lightShadow" id="sponsorButton">Become a Sponsor</button></p>
    </div>
  </section>

  <!-- Sponsor Sign Up  -->
  <section class="sectionSSU">
    <div id="sectionSSU_close">&times;</div>
    <div class="container text-center">
      <h2 class="text-white">Sponsor Packages</h2>
      <ul class="ul-nolist">
        <li class="sectionSSU_package">
          <div class="sectionSSU_header">
            <img src="<?php bloginfo('template_url') ?>/assets/img/pkg/bronze.png" alt="">
            <h3 class="heading-h4">Bronze</h3>
          </div>
          <div class="sectionSSU_body">
            <p><?php echo $bronzeBenefits; ?></p>
          </div>
          <div class="sectionSSU_price">
             <p class="heading-h3">$<?php echo $bronzePrice; ?></p>
           </div>
          <button class="btn btn-block btn-purple" data-value="Bronze">Select</button>
        </li>
        <li class="sectionSSU_package">
          <div class="sectionSSU_header">
            <img src="<?php bloginfo('template_url') ?>/assets/img/pkg/silver.png" alt="">
            <h3 class="heading-h4">Silver</h3>
          </div>
          <div class="sectionSSU_body">
            <p><?php echo $silverBenefits; ?></p>
          </div>
          <div class="sectionSSU_price">
             <p class="heading-h3">$<?php echo $silverPrice; ?></p>
           </div>
          <button class="btn btn-block btn-purple" data-value="Silver">Select</button>
        </li>
        <li class="sectionSSU_package">
          <div class="sectionSSU_header">
            <img src="<?php bloginfo('template_url') ?>/assets/img/pkg/gold.png" alt="">
            <h3 class="heading-h4">Gold</h3>
          </div>
          <div class="sectionSSU_body">
            <p><?php echo $goldBenefits; ?></p>
          </div>
          <div class="sectionSSU_price">
            <p class="heading-h3">$<?php echo $goldPrice; ?></p>
          </div>
          <button class="btn btn-block btn-purple" data-value="Gold">Select</button>
        </li>
      </ul>

      <?php echo $sponsorForm ; ?>

    </div>

  </section>

<?php endif; ?>

  <!-- Contact Us Section  -->
  <section class="section sectionContact section-padding100 text-white">
    <div class="container">
      <h2><?php echo $contactSecton; ?></h2>
      <?php echo $contactForm; ?>

      <!-- Footer  -->
      <footer class="footer">
        <ul class="social ul-nolist">
        <?php if (!empty($facebook)): ?>
          <li><a class="social_fb" target="_blank" href="https://<?php echo $facebook; ?>">facebook</a></li>
        <?php endif;?>

        <?php if (!empty($instagram)): ?>
          <li><a class="social_insta" target="_blank" href="https://<?php echo $instagram; ?>">instagram</a></li>
        <?php endif;?>

        <?php if (!empty($twitter)): ?>
          <li><a class="social_twitter" target="_blank" href="https://<?php echo $twitter; ?>">twitter</a></li>
        <?php endif;?>

        <?php if (!empty($googlePlus)): ?>
          <li><a class="social_google" target="_blank" href="https://<?php echo $googlePlus; ?>">google plus</a></li>
        <?php endif;?>
        </ul>
        <p class="footer_copyright">&copy; <?php echo date('Y'); ?> University of Technology</p>
      </footer>
    </div>
  </section>

<?php
get_footer();
