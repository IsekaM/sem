<?php

$schoolName	= get_field('main_page_school_name');
$eventName 	= get_field('main_page_event_name');

get_header();
?>


<main id="pageMain">
	<div class="main_heading">
		<div class="container text-white text-center">
			<h1><span class="main_heading-small"><?php echo $schoolName; ?></span><br><?php echo $eventName; ?></h1>
		</div>
	</div>

	<div class="container adjust">
		<ul class="row tabSection ul-nolist">

			<?php $loop = new WP_Query( array( 'post_type' => 'seminar_pages', 'orderby' => 'post_id', 'order' => 'ASC' ) );  ?>
			<?php while ($loop->have_posts()): $loop->the_post(); ?>

			<li class="col-lg-4 col-sm-6 col-xs-12 tab_container">
				<a href="http://<?php echo get_field('link_to_page'); ?>"></a>
				<div class="tab" style="background: url(<?php echo get_field('sem_feature_image'); ?>) top/100% no-repeat;">
					<p class="tab_semName p-noMargin"><?php echo get_field('seminar_name'); ?></p>
				</div>
			</li>

		<?php endwhile; ?>
		</ul>
	</div>

</main>

<script type="text/javascript">
	jQuery(function () {
		goodHeightRatio('.tab_container', 0.6);

		$window.on('resize', function () {
			goodHeightRatio('.tab_container', 0.6);
		});
	});
</script>
<?php
get_footer();
