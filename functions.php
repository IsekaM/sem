<?php
/**
 * Utech Seminar functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Utech_Seminar
 */
define( 'ACF_LITE', true );
include_once('plugins/advanced-custom-fields/acf.php');

if ( ! function_exists( 'utech_seminar_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function utech_seminar_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Utech Seminar, use a find and replace
		 * to change 'utech-seminar' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'utech-seminar', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'utech-seminar' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'utech_seminar_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'utech_seminar_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function utech_seminar_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'utech_seminar_content_width', 640 );
}
add_action( 'after_setup_theme', 'utech_seminar_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function utech_seminar_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'utech-seminar' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'utech-seminar' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'utech_seminar_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function utech_seminar_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/styles/bootstrap-grid.min.css');

	wp_enqueue_style( 'font', get_template_directory_uri() . '/assets/font/font.css');

	wp_enqueue_style( 'utech-seminar-style', get_stylesheet_uri(), array('bootstrap') );

	wp_enqueue_script( 'utech-seminar-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'utech-seminar-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCTAQZ0gNxCGM7k1tOnrAlQE9_K46NvtLY', array(), '3', true );

	wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/assets/js/map.js', array('google-map', 'jquery'), '0.1', true );

	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0', false );

	if (is_page_template( 'page-home.php' )) {
		wp_enqueue_script( 'homepage', get_template_directory_uri() . '/assets/js/homepage.js', array('jquery', 'main-script'), false, true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'utech_seminar_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/****************************************************************************
	Plugin Verification
****************************************************************************/

require get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'register_required_plugins' );

function register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.

		array(
			'name'               => 'Contact Form 7', // The plugin name.
			'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/plugins/contact-form-7.5.0.1.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '5.0.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
		),
	);

	$theme_text_domain = 'utech-seminar';

	$config = array(
		'domain'       => $theme_text_domain,         // Text domain - likely want to be the same as your theme.
		/*'default_path' => '',                         // Default absolute path to pre-packaged plugins */
		'menu'         => 'install-required-plugins', // Menu slug
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'is_automatic' => true,
		'strings'      	 => array(
			'page_title'             => __( 'Install Required Plugins', $theme_text_domain ), //
			'menu_title'             => __( 'Install Plugins', $theme_text_domain ), //
			'instructions_install'   => __( 'The %1$s plugin is required for this theme. Click on the big blue button below to install and activate %1$s.', $theme_text_domain ), // %1$s = plugin name
			'instructions_activate'  => __( 'The %1$s is installed but currently inactive. Please go to the <a href="%2$s">plugin administration page</a> page to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL
			'button'                 => __( 'Install %s Now', $theme_text_domain ), // %1$s = plugin name
			'installing'             => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
			'oops'                   => __( 'Something went wrong with the plugin API.', $theme_text_domain ), //
			'notice_can_install'     => __( 'This theme requires the %1$s plugin. <a href="%2$s"><strong>Click here to begin the installation process</strong></a>. You may be asked for FTP credentials based on your server setup.', $theme_text_domain ), // %1$s = plugin name, %2$s = TGMPA page URL */
			'notice_cannot_install'  => __( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', $theme_text_domain ), // %1$s = plugin name
			'notice_can_activate'    => __( 'This theme requires the %1$s plugin. That plugin is currently inactive, so please go to the <a href="%2$s">plugin administration page</a> to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL
			'notice_cannot_activate' => __( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', $theme_text_domain ), // %1$s = plugin name
			'return'                 => __( 'Return to Required Plugins Installer', $theme_text_domain ), //
		),
	);

	tgmpa( $plugins, $config );
}

/****************************************************************************
 Custom Post Types
****************************************************************************/
function cptui_register_my_cpts() {

	/**
	 * Post Type: Speakers.
	 */

	$labels = array(
		"name" => __( "Speakers", "utech-seminar" ),
		"singular_name" => __( "Speaker", "utech-seminar" ),
	);

	$args = array(
		"label" => __( "Speakers", "utech-seminar" ),
		"labels" => $labels,
		"description" => "Add the speakers and speaker info.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "speakers", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-microphone",
		"supports" => array( "title" ),
	);

	register_post_type( "speakers", $args );

	/**
	 * Post Type: Events.
	 */

	$labels = array(
		"name" => __( "Events", "utech-seminar" ),
		"singular_name" => __( "Event", "utech-seminar" ),
	);

	$args = array(
		"label" => __( "Events", "utech-seminar" ),
		"labels" => $labels,
		"description" => "Highlight  all the events that will be held on the day of the seminar.",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "events", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-tickets-alt",
		"supports" => array( "title" ),
	);

	register_post_type( "events", $args );

	/**
	 * Post Type: Sponsors.
	 */

	$labels = array(
		"name" => __( "Sponsors", "utech-seminar" ),
		"singular_name" => __( "Sponsor", "utech-seminar" ),
	);

	$args = array(
		"label" => __( "Sponsors", "utech-seminar" ),
		"labels" => $labels,
		"description" => "All the sponsors that contributed to this event\'s happening ",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "sponsors", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-building",
		"supports" => array( "title" ),
	);

	register_post_type( "sponsors", $args );

	/**
	 * Post Type: Seminar Pages.
	 */

	$labels = array(
		"name" => __( "Seminar Pages", "utech-seminar" ),
		"singular_name" => __( "Seminar Page", "utech-seminar" ),
	);

	$args = array(
		"label" => __( "Seminar Pages", "utech-seminar" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "seminar_pages", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-media-default",
		"supports" => array( "title" ),
	);

	register_post_type( "seminar_pages", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );



/****************************************************************************
 	Remove Unnecessary Menus
****************************************************************************/

function remove_posts_menu() {
    remove_menu_page('edit.php');
		remove_menu_page( 'edit-comments.php' );
}

add_action('admin_menu', 'remove_posts_menu');



/****************************************************************************
	Advanced Custom Fields
****************************************************************************/
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_events',
		'title' => 'Events',
		'fields' => array (
			array (
				'key' => 'field_5a9a0393134bb',
				'label' => 'Event Feature Image',
				'name' => 'event_image',
				'type' => 'image',
				'instructions' => 'An image that depicts what will happen at the event',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a9a02f8134ba',
				'label' => 'Event Description',
				'name' => 'event_description',
				'type' => 'textarea',
				'instructions' => 'A short paragraph describing the event.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 10,
				'formatting' => 'none',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_homepage',
		'title' => 'Homepage',
		'fields' => array (
			array (
				'key' => 'field_5a99d2dc3055d',
				'label' => 'Header Section',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5a99d1d465cd4',
				'label' => 'Event Feature Image',
				'name' => 'event_feature_image',
				'type' => 'image',
				'instructions' => 'Please ensure your image is 870px wide and 870px long',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a99d08b65cd0',
				'label' => 'School Name',
				'name' => 'school_name',
				'type' => 'text',
				'instructions' => 'Enter the name of school.',
				'required' => 1,
				'default_value' => 'University of Technology',
				'placeholder' => 'eg. University of Technology',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99d0dd65cd1',
				'label' => 'Seminar Name',
				'name' => 'seminar_name',
				'type' => 'text',
				'instructions' => 'Enter the name of the seminar',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. Accounting Seminar',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99d17465cd3',
				'label' => 'Address of Event',
				'name' => 'address_of_event',
				'type' => 'text',
				'instructions' => 'Enter the address of the event location',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. 237 Old Hope Road, Kingston',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99d11b65cd2',
				'label' => 'Date of Seminar',
				'name' => 'date_of_seminar',
				'type' => 'text',
				'instructions' => 'Enter the date of the event.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. March 28, 2018',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99d94a0cea4',
				'label' => 'About Us',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5a99d96a0cea5',
				'label' => 'Title',
				'name' => 'about_us_title',
				'type' => 'text',
				'instructions' => 'Enter a title for the "About Us" section.',
				'required' => 1,
				'default_value' => 'About the Seminar',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99d9d00cea6',
				'label' => 'Body',
				'name' => 'about_us_body',
				'type' => 'textarea',
				'instructions' => 'Interesting details about the seminar',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5a99e10dbf67f',
				'label' => 'Google Maps',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5a99e135bf680',
				'label' => 'Section Title',
				'name' => 'map_section_title',
				'type' => 'text',
				'instructions' => 'Enter a title for this section.',
				'required' => 1,
				'default_value' => 'Location',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99e22cbf681',
				'label' => 'Map',
				'name' => 'map',
				'type' => 'google_map',
				'required' => 1,
				'center_lat' => '',
				'center_lng' => '',
				'zoom' => '',
				'height' => '',
			),
			array (
				'key' => 'field_5a9a530d0e669',
				'label' => 'Section Title',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5a9a533c0e66a',
				'label' => 'Speaker Section',
				'name' => 'speaker_section_heading',
				'type' => 'text',
				'default_value' => 'Speakers',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a537f0e66b',
				'label' => 'Event Section',
				'name' => 'event_section_heading',
				'type' => 'text',
				'default_value' => 'Events',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a54150e66d',
				'label' => 'Contact Section',
				'name' => 'contact_section_heading',
				'type' => 'text',
				'default_value' => 'Contact Us',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a0d9e477cc',
				'label' => 'Social Media Links',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_5a9a0dbb477cd',
				'label' => 'Facebook',
				'name' => 'facebook',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Eg. facebook.com/jarenegades',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a0dfd477ce',
				'label' => 'Instagram',
				'name' => 'instagram',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Eg. instagram.com/jarenegades/',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a0e46477cf',
				'label' => 'Twitter',
				'name' => 'twitter',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Eg. twitter.com/jarenegades',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a0e6b477d0',
				'label' => 'Google Plus',
				'name' => 'google_plus',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'Eg. plus.google.com/+jarenegades',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_main-page',
		'title' => 'Main Page',
		'fields' => array (
			array (
				'key' => 'field_5ab4418768718',
				'label' => 'School Name',
				'name' => 'main_page_school_name',
				'type' => 'text',
				'default_value' => 'University of Technology',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5ab441d468719',
				'label' => 'Event Name',
				'name' => 'main_page_event_name',
				'type' => 'text',
				'default_value' => 'Seminars',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'default',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_seminar-pages',
		'title' => 'Seminar Pages',
		'fields' => array (
			array (
				'key' => 'field_5ab444de30b77',
				'label' => 'Feature Image',
				'name' => 'sem_feature_image',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_5ab4453730b78',
				'label' => 'Seminar Name',
				'name' => 'seminar_name',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. Accounting',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5ab4563dcbd00',
				'label' => 'Link to Page',
				'name' => 'link_to_page',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'www.example.com/something',
				'prepend' => 'http://',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'seminar_pages',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_speakers',
		'title' => 'Speakers',
		'fields' => array (
			array (
				'key' => 'field_5a99f6635f00a',
				'label' => 'Speaker Image',
				'name' => 'speaker_image',
				'type' => 'image',
				'instructions' => 'Choose the image of the speaker',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a99f7b25f00c',
				'label' => 'Speaker Name',
				'name' => 'speaker_name',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. John Doe',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99f7265f00b',
				'label' => 'Speaker\'s Workplace/Company',
				'name' => 'speaker_company',
				'type' => 'text',
				'instructions' => 'Enter the company where the speaker is employed',
				'default_value' => '',
				'placeholder' => 'Eg. Grace Kennedy',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a99f7e95f00d',
				'label' => 'Speaker\'s Position',
				'name' => 'speaker_position',
				'type' => 'text',
				'instructions' => 'The position the speaker is appointed to at his/her company/workplace',
				'default_value' => '',
				'placeholder' => 'Eg. General Manager',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'speakers',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_sponsors',
		'title' => 'Sponsors',
		'fields' => array (
			array (
				'key' => 'field_5a9a09525decf',
				'label' => 'Sponsor\'s Logo',
				'name' => 'sponsor_logo',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5a9a09be5ded1',
				'label' => 'Sponsor\'s Website',
				'name' => 'sponsor_website',
				'type' => 'text',
				'instructions' => 'Please include "http" or "https" when pasting or typing the link.',
				'default_value' => '',
				'placeholder' => 'Eg. www.digicel.com',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sponsors',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_packages',
		'title' => 'Packages',
		'fields' => array (
			array (
				'key' => 'field_5a9d0adf2d2fa',
				'label' => 'Add Sponsor Section',
				'name' => 'add_sponsor_section',
				'type' => 'true_false',
				'message' => 'Do you want to include sponsor section on your website',
				'default_value' => 1,
			),
			array (
				'key' => 'field_5a9d0a18bbc67',
				'label' => 'Sponsor Section Heading',
				'name' => 'sponser_section_heading',
				'type' => 'text',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5a9d0adf2d2fa',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Sponsors',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9a4cbb7c39e',
				'label' => 'Bronze',
				'name' => '',
				'type' => 'tab',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5a9d0adf2d2fa',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
			),
			array (
				'key' => 'field_5a9a4cd27c39f',
				'label' => 'Package Price',
				'name' => 'bronze_price',
				'type' => 'number',
				'instructions' => 'Price of package',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. $95,000',
				'prepend' => '$',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5a9a4d2e7c3a0',
				'label' => 'Package Benefits',
				'name' => 'bronze_benefits',
				'type' => 'textarea',
				'instructions' => 'Benefits the sponsor will get by choosing this particular package.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_5a9a4d857c3a1',
				'label' => 'Silver',
				'name' => '',
				'type' => 'tab',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5a9d0adf2d2fa',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
			),
			array (
				'key' => 'field_5a9a4d937c3a2',
				'label' => 'Package Price',
				'name' => 'silver_price',
				'type' => 'number',
				'instructions' => 'Price of package',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. $95,000',
				'prepend' => '$',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5a9a4d9c7c3a3',
				'label' => 'Package Benefits',
				'name' => 'silver_benefits',
				'type' => 'textarea',
				'instructions' => 'Benefits the sponsor will get by choosing this particular package.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_5a9a4dd57c3a4',
				'label' => 'Gold',
				'name' => '',
				'type' => 'tab',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5a9d0adf2d2fa',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
			),
			array (
				'key' => 'field_5a9a4e0e7c3a6',
				'label' => 'Package Price',
				'name' => 'gold_price',
				'type' => 'number',
				'instructions' => 'Price of package',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'Eg. $95,000',
				'prepend' => '$',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5a9a4de77c3a5',
				'label' => 'Package Benefits',
				'name' => 'gold_benefits',
				'type' => 'textarea',
				'instructions' => 'Benefits the sponsor will get by choosing this particular package.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'none',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 20,
	));
}


/****************************************************************************
	Advanced Custom Fields
****************************************************************************/

function my_acf_google_map_api($api){

	$api['key'] = 'AIzaSyCTAQZ0gNxCGM7k1tOnrAlQE9_K46NvtLY';

	return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
