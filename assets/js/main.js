function heightEqual(object) {
  var headerImgWidth = jQuery(object).width();
  jQuery(object).css('height', headerImgWidth);
}

function fixOverflow() {
  jQuery('body, html').css('overflow-x', 'hidden');
}

function goodHeightRatio(object, size = 1.166666667) {
  var $object = jQuery(object);
  var objectWidth = $object.width();
  $object.css('height', objectWidth * size);
}

function scrollTo(item, obj) {
  var $top = jQuery(obj).offset().top;
  jQuery(item).on('click', function (e) {
    e.preventDefault();
    jQuery('body, html').animate({
      scrollTop: $top
    }, 1000);
  });
}

var $window = jQuery(window);
var windowWidth = $window.width();
var scrollBarHeight = $window.scrollTop();
