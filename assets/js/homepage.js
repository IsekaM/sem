jQuery(function () {
  heightEqual('.header_img');
  goodHeightRatio('.sem-img', 0.5526315789473684);
  goodHeightRatio('.tab_container', 0.6);
  goodHeightRatio('.speaker_img');
  scrollTo('#learnMore', 'section:first');

  jQuery('#sponsorButton, #sectionSSU_close').on('click', function () {
    jQuery('.sectionSSU').toggleClass('js_showSection');
  });

  $window.on('resize', function () {
    heightEqual('.header_img');
    goodHeightRatio('.sem-img', 0.5526315789473684);
    goodHeightRatio('.tab_container', 0.6);
    goodHeightRatio('.speaker_img');
  });

  $window.on('scroll', function () {
    var scrollPos = $window.scrollTop();
    jQuery('.js_para').each(function () {
      var $obj = jQuery(this);
      var objTop = $obj.offset().top;
      var yPos = -(($window.scrollTop() - objTop) / $obj.data('speed'));
      $obj.css({ transform: 'translateY(' + yPos + 'px)' });
    });

    scrollBarHeight = $window.scrollTop();
  });

  jQuery('.btn-purple').on('click', function () {
    var top = jQuery('form:first').offset().top;
    jQuery('#sponsor_package').val(jQuery(this).data('value')).trigger('change');
    jQuery('.sectionSSU').animate({
      scrollTop: top
    }, 1200);
  });
});
